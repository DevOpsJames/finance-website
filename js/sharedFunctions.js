export function debounce (fn, wait = 1) {
  let timeout
  return function (...args) {
    clearTimeout(timeout)
    timeout = setTimeout(() => fn.call(this, ...args), wait)
  }
}

export function assignAssumptionParamsFromClipboard(urlParams, assumptions){
  if (urlParams.has('roi')) assumptions.roi = doubleFromMoneyString(urlParams.get('roi'));
  if (urlParams.has('inflation')) assumptions.inflation = doubleFromMoneyString(urlParams.get('inflation'));
  if (urlParams.has('retirementIncomeTaxRate')) assumptions.retirementIncomeTaxRate = doubleFromMoneyString(urlParams.get('retirementIncomeTaxRate'));
  if (urlParams.has('ssAge')) assumptions.ssAge = doubleFromMoneyString(urlParams.get('ssAge'));
  if (urlParams.has('socialSecurityIncome')) assumptions.socialSecurityIncome = doubleFromMoneyString(urlParams.get('socialSecurityIncome'))
  if (urlParams.has('pensionAge')) assumptions.pensionAge = doubleFromMoneyString(urlParams.get('pensionAge'));
  if (urlParams.has('fixedIncome')) assumptions.fixedIncome = doubleFromMoneyString(urlParams.get('fixedIncome'));
  if (urlParams.has('deathAge')) assumptions.deathAge = doubleFromMoneyString(urlParams.get('deathAge'));
  if (urlParams.has('estateSize')) assumptions.estateSize = doubleFromMoneyString(urlParams.get('estateSize'));
}

export function doubleFromMoneyString(moneyString){
  let commasRemoved = moneyString.replace(/[^0-9.\-]/g, "");
  let parsedString = parseFloat(commasRemoved);
  if (isNaN(parsedString)) return 0;

  return parsedString;
}
  
export function moneyStringFromDouble(double) {
  let wholeNumberSetting = new Intl.NumberFormat(Intl.Locale.baseName, {
    minimumFractionDigits: 0,
  });

  let fractionalNumberSetting = new Intl.NumberFormat(Intl.Locale.baseName, {
    minimumFractionDigits: 2,
  })

  if (double % 1 == 0)
    return wholeNumberSetting.format(double);
  else
    return fractionalNumberSetting.format(double);
}
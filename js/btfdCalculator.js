import Vue from 'vue'
import 'bootstrap';
import BootstrapVue from "bootstrap-vue"
import BtfdCalculator from '../components/btfd-calculator.vue'
import NumberInput from '../components/number-input.vue'
//import SquareResponsiveAd from '../components/square-responsive-ad.vue'
import SquareResponsiveAdSense from '../components/square-responsive-adsense.vue'

Vue.use(BootstrapVue);
Vue.component('number-input', NumberInput);
//Vue.component('square-responsive-ad', SquareResponsiveAd);
Vue.component('square-responsive-adsense', SquareResponsiveAdSense);

var app = new Vue({
  el: '#calculator',
  render: h => h(BtfdCalculator)
});
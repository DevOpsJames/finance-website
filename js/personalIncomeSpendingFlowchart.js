import Vue from 'vue'
import 'bootstrap';
import BootstrapVue from "bootstrap-vue"
import Guide from '../components/personal-income-spending-flowchart.vue'

Vue.use(BootstrapVue);

var app = new Vue({
  el: '#calculator',
  render: h => h(Guide)
});
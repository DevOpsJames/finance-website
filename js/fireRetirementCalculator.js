import Vue from 'vue'
import 'bootstrap';
import BootstrapVue from "bootstrap-vue"
import FireCalculator from '../components/fire-retirement-calculator.vue'
import NumberInput from '../components/number-input.vue'
//import SquareResponsiveAd from '../components/square-responsive-ad.vue'
import SquareResponsiveAdSense from '../components/square-responsive-adsense.vue'
import Assumptions from '../components/assumptions.vue'
import FireCalculatorSummary from '../components/fire-retirement-summary.vue'

Vue.use(BootstrapVue);
Vue.component('number-input', NumberInput);
//Vue.component('square-responsive-ad', SquareResponsiveAd);
Vue.component('square-responsive-adsense', SquareResponsiveAdSense);
Vue.component('assumptions', Assumptions);
Vue.component('fire-retirement-summary', FireCalculatorSummary);

var app = new Vue({
  el: '#calculator',
  render: h => h(FireCalculator)
});
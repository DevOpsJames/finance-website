import Vue from 'vue'
import 'bootstrap';
import BootstrapVue from "bootstrap-vue"
import MortgageCalculator from '../components/mortgage-calculator.vue'
import AdSense from '../components/in-article-adsense.vue'

Vue.use(BootstrapVue);
Vue.component('adsense', AdSense);

var app = new Vue({
  el: '#calculator',
  render: h => h(MortgageCalculator)
});
import Chart from 'chart.js';
import { moneyStringFromDouble } from '../js/sharedFunctions.js';

export function generateNewProof(desiredLifestyle, retirementIncomeTaxRate, currentSavings, startSaving, roi, inflation, currentAge, retirementAge, deathAge, ssAge, pensionAge, socialSecurityIncome, fixedIncome) {
  let newProof = [];
  desiredLifestyle /= (1 - retirementIncomeTaxRate);
  let interestOnSavings = currentSavings * roi;

  for (let i = currentAge; i < deathAge; i++) {
    let currentYearRetirementSocialSecurityAndFixedIncome = 0;
    if (i >= ssAge) {
      currentYearRetirementSocialSecurityAndFixedIncome += socialSecurityIncome;
    }
    if (i >= pensionAge) {
      currentYearRetirementSocialSecurityAndFixedIncome += fixedIncome;
    }

    let thisNewProof = { 
      age: i, 
      savings: Math.round(currentSavings), 
      contribution: null, 
      interest: Math.round(interestOnSavings), 
      otherIncome: Math.round(currentYearRetirementSocialSecurityAndFixedIncome),
      netGain: null
    };
    if (i < retirementAge) {
      thisNewProof.contribution = Math.round(startSaving + currentYearRetirementSocialSecurityAndFixedIncome);
      thisNewProof.netGain = Math.round(startSaving + interestOnSavings + currentYearRetirementSocialSecurityAndFixedIncome);
    } else {
      thisNewProof.contribution = Math.round(-desiredLifestyle);
      thisNewProof.netGain = Math.round(-desiredLifestyle + interestOnSavings + currentYearRetirementSocialSecurityAndFixedIncome);
    }
    newProof.push(thisNewProof);

    if (i >= ssAge) {
      currentSavings += socialSecurityIncome;
      socialSecurityIncome *= 1 + inflation;
    }

    if (i >= pensionAge) {
      currentSavings += fixedIncome;
    }

    if (i < retirementAge) {
      currentSavings += startSaving;
      startSaving *= 1 + inflation;
    } else {
      currentSavings -= desiredLifestyle;
    }

    currentSavings += interestOnSavings;
    interestOnSavings = currentSavings * roi;

    desiredLifestyle *= 1 + inflation;
  }

  newProof.push({ 
    age: deathAge, 
    savings: Math.round(currentSavings), 
    contribution: 0, 
    interest: Math.round(interestOnSavings), 
    otherIncome: 0,
    netGain: 0
  });

  return newProof;
}

export function generateGraphs(balanceChartElement, incomeChartElement, newProof, retirementAge, deathAge) {
  //balance chart
  Chart.defaults.global.elements.point.pointStyle = 'line';
  var xAxis = newProof.map(function(item) {
    return item.age;
  });      
  var yAxis = newProof.map(function(item) {
    return item.savings;
  });
  
  if (typeof window.balanceChartObj !== 'undefined')
  {
    window.balanceChartObj.data.datasets[0].data = yAxis;
    window.balanceChartObj.data.labels = xAxis;
    window.balanceChartObj.update();
  }
  else
  {
    window.balanceChartObj = new Chart(balanceChartElement, {
      type: 'line',
      data: {
        labels: xAxis,
        datasets: [{
          cubicInterpolationMode: 'monotone',
          pointRadius: 0,
          data: yAxis,
          backgroundColor: [
            'rgba(40, 167, 69, 0.2)'
          ],
          borderColor: [
            'rgba(40, 167, 69, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        legend: {
          display: false
        },
        events: [],
        plugins: {
          filler: {
            propagate: false
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              minRotation: 0,
              maxRotation: 0,
              beginAtZero: true,
              callback: function(value, index, values) {
                return '$' + moneyStringFromDouble(value);
              }
            }
          }],
          xAxes: [{
            ticks: {
              minRotation: 50,
              maxRotation: 50
            }
          }]
        },
        animation: {
          duration: 0 // general animation time
        },
        hover: {
          animationDuration: 0 // duration of animations when hovering an item
        },
        responsiveAnimationDuration: 0, // animation duration after a resize
        elements: {
          line: {
            tension: 0 // disables bezier curves
          }
        }
      }
    });
  }
  
  //income chart
  var incomeChartData = newProof.filter(item => item.age >= retirementAge && item.age < deathAge);

  var incomeXAxis = incomeChartData.map(function(item) {
    return item.age;
  });      
  var saleOfAssetsAxis = incomeChartData.map(function(item) {
    let result = Math.max(0, -item.netGain);
    let interestAxis = item.interest - Math.max(0, item.netGain);
    if (interestAxis < 0) {
      result += interestAxis;
    }
    return result;
  });
  var interestAxis = incomeChartData.map(function(item) {
    return item.interest - Math.max(0, item.netGain);
  });
  var otherIncomeAxis = incomeChartData.map(function(item) {
    return item.otherIncome + Math.min(0, item.interest - item.netGain);
  });
  
  if (typeof window.incomeChartObj !== 'undefined')
  {
    window.incomeChartObj.data.labels = incomeXAxis;
    window.incomeChartObj.data.datasets[0].data = saleOfAssetsAxis;
    window.incomeChartObj.data.datasets[1].data = interestAxis;
    window.incomeChartObj.data.datasets[2].data = otherIncomeAxis;
    window.incomeChartObj.update();
  }
  else
  {
    window.incomeChartObj = new Chart(incomeChartElement, {
      type: 'line',
      data: {
        labels: incomeXAxis,
        datasets: [{
          cubicInterpolationMode: 'monotone',
          pointRadius: 0,
          label: 'capital drawdown',
          data: saleOfAssetsAxis,
          backgroundColor: [
            'rgba(40, 167, 69, 0.2)'
          ],
          borderColor: [
            'rgba(40, 167, 69, 1)'
          ],
          borderWidth: 1
        },
        {
          cubicInterpolationMode: 'monotone',
          pointRadius: 0,
          label: 'capital growth',
          data: interestAxis,
          backgroundColor: [
            'rgba(140, 67, 69, 0.2)'
          ],
          borderColor: [
            'rgba(140, 67, 69, 1)'
          ],
          borderWidth: 1
        },
        {
          cubicInterpolationMode: 'monotone',
          pointRadius: 0,
          label: 'social security/pension',
          data: otherIncomeAxis,
          backgroundColor: [
            'rgba(40, 67, 169, 0.2)'
          ],
          borderColor: [
            'rgba(40, 67, 169, 1)'
          ],
          borderWidth: 1
        }],
      },
      options: {
        events: [],
        plugins: {
          filler: {
            propagate: false
          }
        },
        scales: {
          yAxes: [{
            stacked: true,
            ticks: {
              minRotation: 0,
              maxRotation: 0,
              beginAtZero: true,
              callback: function(value, index, values) {
                  return '$' + moneyStringFromDouble(value);
              }
            }
          }],
          xAxes: [{
            ticks: {
              minRotation: 50,
              maxRotation: 50
            }
          }]
        },
        animation: {
          duration: 0 // general animation time
        },
        hover: {
          animationDuration: 0 // duration of animations when hovering an item
        },
        responsiveAnimationDuration: 0, // animation duration after a resize
        elements: {
          line: {
            tension: 0 // disables bezier curves
          }
        }
      }
    });
  }
}
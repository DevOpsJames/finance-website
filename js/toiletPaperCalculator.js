import Vue from 'vue'
import 'bootstrap';
import BootstrapVue from "bootstrap-vue"
import RetirementCalculator from '../components/toilet-paper-calculator.vue'

Vue.use(BootstrapVue);

var app = new Vue({
  el: '#calculator',
  render: h => h(RetirementCalculator)
});
#! /usr/bin/python

from urllib.request import urlopen
import json, csv

url = 'https://www.ishares.com/us/products/239724/ishares-core-sp-total-us-stock-market-etf/1467271812596.ajax?tab=all&fileType=json'

with urlopen(url) as conn:
  data = json.loads(conn.read())
  with open('../_data/total-us-data.csv', 'w') as csv_file:
    csv_writer = csv.writer(csv_file)
    csv_writer.writerow(['Ticker', 'Name', 'Weight', 'Sector'])
    for holding in data['aaData']:
      csv_writer.writerow([
        holding[0], 
        holding[1], 
        holding[5]['display'] + '%',
        holding[2],
        ])

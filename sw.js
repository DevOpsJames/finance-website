importScripts('https://storage.googleapis.com/workbox-cdn/releases/5.1.2/workbox-sw.js');

workbox.loadModule('workbox-strategies');
workbox.loadModule('workbox-expiration');
workbox.loadModule('workbox-cacheable-response');

let cacheName = 'nesteggly-cache';

self.addEventListener('fetch', (event) => {
  if (event.request.method === 'GET' &&
    event.request.url.startsWith(self.location.origin)) {
    const networkFirst = new workbox.strategies.NetworkFirst({
      cacheName: cacheName,
      plugins: [
        new workbox.expiration.ExpirationPlugin({
          maxAgeSeconds: 24 * 60 * 60,
        }),
        new workbox.cacheableResponse.CacheableResponsePlugin({
          statuses: [0, 200]
        })
      ]
    });
    event.respondWith(networkFirst.handle({request: event.request}));
  }
});

self.addEventListener('install', (event) => {
  const urls = [ '/', '/img/egg-green-bg-22x30.gif' ];
  event.waitUntil(caches.open(cacheName).then((cache) => cache.addAll(urls)));
});
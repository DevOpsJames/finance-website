const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const ManifestPlugin = require('webpack-manifest-plugin');  
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const JavaScriptObfuscator = require('webpack-obfuscator');

module.exports = {
  mode: 'none',
  performance: {
    hints: false // disable bundle size warnings.
  },
  plugins: [
    new VueLoaderPlugin(),
    new ManifestPlugin({
        fileName: '../_data/manifest.yml',  
        publicPath: '/webpack-js/',
    }),
    new CleanWebpackPlugin()
  ],
  entry: {
    mortgageCalculatorBundle: './js/mortgagecalculator.js',
    retirementCalculatorBundle: './js/retirementCalculator.js',
    investmentWithdrawalCalculatorBundle: './js/investmentWithdrawalCalculator.js',
    fireRetirementCalculatorBundle: './js/fireRetirementCalculator.js',
    toiletPaperCalculatorBundle: './js/toiletPaperCalculator.js',
    btfdCalculatorBundle: './js/btfdCalculator.js',
    personalIncomeSpendingFlowchartBundle: './js/personalIncomeSpendingFlowchart.js',
    justTheBasicsBundle: './js/justTheBasics.js'
  },
  output: {
    path: path.resolve(__dirname, 'webpack-js'),
    filename: '[name].[contenthash:7].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [ // use `include` vs `exclude` to white-list vs black-list
                    path.resolve(__dirname, "src"), // white-list your app source files
                    require.resolve("bootstrap-vue"), // white-list bootstrap-vue
                ],
        loader: 'babel-loader',
      },
      {
        test: /\.json$/,
        use: 'json-loader'
      },
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      {
        test: /\.css$/,
        loader: "vue-style-loader!css-loader"
      }
    ]
  },
  resolve: {
    alias: {
      jquery: "jquery/dist/jquery.slim.js" // Bootstrap doesn't need the full version
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  optimization: {
    moduleIds: 'hashed',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
  }
};
console.log("this is the env: " + process.env.NODE_ENV);

if (process.env.NODE_ENV == 'production') {
  module.exports.output = {
    path: path.resolve(__dirname, 'webpack-js'),
    filename: '[name].[contenthash:7].js'
  };
  module.exports.plugins.push(
    new JavaScriptObfuscator({
      seed: 1959315703,
    }, ['**vendors*.js'])
  );
  module.exports.optimization.minimizer = [
    new TerserPlugin({
      terserOptions: {
        toplevel: true,
        compress: {
          passes: 2
        },
        mangle: {
          properties: {
              regex: /(^P1|^p1|^_p1)[A-Z]\w*/
          }
        },
        output: {
          comments: false,
        },
      },
    }),
  ];
} else {
  module.exports.output = {
    path: path.resolve(__dirname, 'webpack-js'),
    filename: '[name].js'
  };

  module.exports.devtool = "source-map";
  module.exports.output.sourceMapFilename = "[name].js.map";
}

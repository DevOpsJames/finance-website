---
layout: post
title: If you have a mortgage and own bonds, read this right now
excerpt: In this post we debunk the myth that every portfolio needs bonds.
metadescription: A popular myth is that everyone should have some allocation of bonds in their portfolio. We consider the ramifications of owning bonds while also holding a mortgage.
heroimage: /img/egg-landscape-cropped-vertically.jpg
ogimage: https://www.nesteggly.com/img/egg-landscape-cropped-vertically.jpg
ogtitle: If you have a mortgage and own bonds, read this right now
published: false
---

-----



It sounds like the strongest argument in favor of 60/40 + mortgage seems to be reduced portfolio volatility. I find this argument flimsy. Why do demand so much stability from your portfolio? Have you considered it might be because you have a mortgage?

Another theme seems to be some folks think the 60/40 strategy actually has alpha, in the sense that it will beat the S&P 500 over the long-term, or that it enables them to time the market or whatever. Frankly, I don't really buy that argument whatsoever. Portfolio stability comes at a cost, and that cost is long-term returns. 

Another myth is 60/40 + mortgage will help you weather surprise unemployment. This is demonstrably false given the following example:
Take a scenario in which stocks go down 50%, bonds down 35%, house value down 50%.

60/40 with mortgage:

Before drop:

Stock value: $600k
Bond value: $400k
Sustainable cashflow from portfolio: $3,300/month
House value: $400k
Mortgage: $320k (payment is $1,500/month)
Spare cashflow from portfolio: $1,800/month

After drop:

Stock value: $300k
Bond value: $260k
Sustainable cashflow from portfolio: $1,970/month
House value: $200k
Mortgage: $320k (payment is $1,500/month)
Spare cashflow from portfolio: $470/month

100/0 without mortgage:

Before drop:

Stock value: $600k
Bond value: $0
Sustainable cashflow from portfolio: $2,442/month
House value: $400k ($0 mortgage)
Mortgage: $0
Spare cashflow from portfolio: $2,442/month

After drop:

Stock value: $300k
Bond value: $0
Sustainable cashflow from portfolio: $1,331/month
House value: $200k
Mortgage: $0
Spare cashflow from portfolio: $1,331/month

This demonstrates the 100% stock (without mortgage) AA provides 183% more post-mortgage cashflow than the 60/40 + mortgage AA.


As someone who's living primarily from one's savings, I feel like the goal should be minimize how rigid your monthly budget is, so that you can afford to take on more volatility (and thus, less cost in the form of buying stability).
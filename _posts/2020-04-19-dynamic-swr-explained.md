---
layout: post
title: Dynamic Safe Withdrawal Rate (SWR), explained
excerpt: The key to making your retirement savings last a lifetime is taking it one day at a time.
metadescription: Dynamic SWR is a dynamic withdrawal strategy which takes into account that the investment horizon shrinks as one ages, and is a compelling alternative to the traditional '4% rule'.
heroimage: /img/egg-landscape-cropped-vertically.jpg
ogimage: https://www.nesteggly.com/img/egg-landscape-cropped-vertically.jpg
metadescription: Dynamic SWR is a dynamic withdrawal strategy which takes into account that the investment horizon shrinks as one ages, and is a compelling alternative to the traditional '4% rule'.
---

<div class="card bg-light mb-3">
  <div class="card-body">
    <p>Key facts about retirement withdrawal strategies:</p>
    <ul>
        <li>The most popular withdrawal strategies tie retirement income to previous years of income.</li>
        <li>Dynamic strategies are a forward-looking alternative.</li>
        <li>Every strategy has its own set of trade-offs which should be carefully considered.</li>
    </ul>
  </div>
</div>

One of the most popular retirement withdrawal strategies today is the '[4% rule](http://www.retailinvestor.org/pdf/Bengen1.pdf)'. Popularized in the early 90's by William P. Bengen and again throughout the 00's and 10's by the Trinity Studies, the beauty of the 4% rule is in its simplicity: Your retirement income is determined the day you retire, and henceforth adjusted with inflation.

Historically, the 4% rule has worked out well with nearly a 100% success rate over a 30-year investment horizon as long as your portfolio is structured to provide sufficient growth. However, given recent trends such as slowing global growth, increasing life spans, negative interest rates, and the prevalance of early retirees, the popular 4% rule is being bent in ways never previously imagined.

### Does it bend?

Founded in 1929, Vanguard Wellington Fund Investor Shares (VWELX) is the oldest balanced mutual fund in the United States. Over the years, it's earned its reputation as one of the most trusted places for Americans to stash their retirement savings. Being a balanced mutual fund consisting 50-70% stocks, you wouldn't be crazy to keep the whole of your retirement savings in VWELX.

<img src="/img/four-percent-40-year-sim-final-account-balance.jpg" class="img-fluid" alt="50 simulations of the 4 percent rule since 1930.">
<br/><small><a href="/data/historical-4-pct-swr-sim.xlsx">Download</a> the data.</small>

We ran 50 historical simulations of the 4% rule back to 1930, in which a retiree spends 40 years in retirement instead of the 30 years the 4% rule was designed for. The results were quite startling. If you retired between 1955 and 1973, a span of nearly 20 years, and religously stuck to the 4% rule it's nearly a gaurantee that you exhausted your retirement funds within 40 years. It's also possible that you became fabulously rich (or had lived far below your means, depending how you look at it). There doesn't appear to be much middle ground.

The 4% rule is touted as a safe withdrawal strategy. However, given the dramatic range of potential outcomes, [some say the 4% rule is more of a rule-of-thumb](https://earlyretirementnow.com/2017/07/19/the-ultimate-guide-to-safe-withdrawal-rates-part-17-social-security/) than a bona fide retirement withdrawal strategy.

### Unanchoring from the 4% rule

With the 4% rule, your withdrawal rate is pinned to the inflation-adjusted first withdrawal. In other words, on the day you retire your future income will forever be anchored by the income received on this first day of retirement, regardless of subsequent market returns.

What if withdrawal strategies didn't have such a memory? What might such a strategy look like?

#### The withdrawal amount would be determined by the current amount of savings

In some ways this would make retirement planning simpler. Say, for example, you experience a severe setback in the market or are the beneficiary of windfall. Calculating your new payment should be as simple as providing your latest retirement account balance and life expectancy. By the way, we have a totally free [investment withdrawal calculator](/investment-withdrawal-calculator) which can do exactly that.

In a way, it makes a lot of sense to not kick your portfolio when it's down, and to defer optional spending (like replacing your car or remodeling your home) until the market is outperforming.

The 'sell high' nature of this approach would theoretically have a protective effect on your savings and ensure your savings will last the test of time.

#### The withdrawal amount would increase as the investment horizon wanes

Nobody knows exactly when their time will be up, but you can make a safe bet that it'll probably be some time before your 100th birthday. On the surface this might not sound like useful information, but it is.

You see, there's a whole industry built around helping people hedge their longevity risk in exchange for a lump sum (and, not to mention, various fees). If you're 80 years old and in good health this can turn out to be a good deal since it relieves you having to plan to live beyond 100. As a rule of thumb you can plan as if you're going to live until you're 100 years old, and when you turn 80 purchase an inflation-adjusted immediate annuity at a great rate.

As you're about to see, having an upper bound on the investment horizon allows us to do some fairly interesting things.

### A novel dynamic withdrawal rate approach

Imagine, for a moment, that you **are** an insurance company and you're writing yourself an inflation-adjusted annuity funded by your retirement savings. You would use an annuity formula to determine what your payment should be, and it would probably look like this:

<img src="/img/dynamic-swr-formula.gif" class="img-fluid" alt="The Dynamic SWR formula.">

You could go a step further and factor in other sources of income, like pensions and social security, and write yourself a bespoke annuity which factors in all your sources of income provide a smooth, inflation-adjusted income for the rest of your life. Oh, and did we mention our free [investment withdrawal calculator](/investment-withdrawal-calculator) can do all of that?

### Risks

Over a 50-year retirement, the ideal balance of your investment savings would look something like this:

<img src="/img/ideal-dynamic-swr-drawdown-path.jpg" class="img-fluid" alt="The ideal Dynamic SWR drawdown path.">
<br/><small><a href="/data/historical-dyn-swr-sim.xlsx">Download</a> the data.</small>

However, the reality is that markets are quite volatile. Even in the case of the balanced VWELX fund, the actual path of investment savings could be radically different from the ideal:

<img src="/img/simulated-dynamic-swr-drawdown-path.jpg" class="img-fluid" alt="Simulated historical Dynamic SWR drawdown paths.">
<br/><small><a href="/data/historical-dyn-swr-sim.xlsx">Download</a> the data.</small>

The path of income tells a similar story. The intended income path looks like this:

<img src="/img/ideal-dynamic-swr-income-path.jpg" class="img-fluid" alt="Simulated historical Dynamic SWR drawdown paths.">
<br/><small><a href="/data/historical-dyn-swr-sim.xlsx">Download</a> the data.</small>

And your actual income path may look like any one of these:

<img src="/img/simulated-dynamic-swr-income-path.jpg" class="img-fluid" alt="Simulated historical Dynamic SWR drawdown paths.">
<br/><small><a href="/data/historical-dyn-swr-sim.xlsx">Download</a> the data.</small>

You may notice that the simulated outcomes all ended up being quite a bit higher than the predicted path. This is because in some cases the actual 50-year returns of the market ended up being quite a bit higher than the growth parameter (6%) provided to the withdrawal rate calculation.

### Risk deep dive

You'd be crazy to bet your life savings on a retirement strategy that hasn't been vetted by multiple sources, which is why we partnered with <a href="https://ficalc.app/">FI Calc</a> to perform a comprehensive backtest of the Dynamic SWR strategy.

Using 150 years of market data, we studied 90 retirement cohorts from 1870 through 1960. We simulated what it would be like for each of these 90 cohorts to spend 60 years in retirement using the Dynamic SWR strategy. Here's what we discovered.

#### A hybrid opportunity

One of the most important questions we wanted to answer was how the strategy would hold up during market turbulence, especially at the outset of retirement. This is also known as 'sequence of returns risk'. In other words, we wanted to establish a lower bound for how far your lifestyle could potentially decline.

<img src="/img/dswr-backtest.png" class="img-fluid" alt="Backtest results of Dynamic SWR.">

After running well over 20,000 simulations, we discovered that portfolios consisting of at least 70% equities were able to fall back to the '3% rule' for 100% of our historical retirement cohorts as long as they picked an ROI assumption of no more than 6% and an inflation assumption of no less than 2%, which are the default settings of our calculators. 

Portfolios consisting of between 50% and 70% equities also demonstrated the ability to successfully fall back to the 3% rule 100% of the time, although the ROI assumption had to be dialed back quite a bit, to low as 3.8%.

Portfolios below 50% equities failed to attain the necessary growth to sustain a 3% rule floor over 60 years, regardless of the ROI assumption. This is actually not surprising, given a pure 3% rule strategy also fails this test.

<img src="/img/3-percent-backtest.png" class="img-fluid" alt="Backtest results of Dynamic SWR.">
<br/><small>A pure '3% rule' strategy with a 40/60 portfolio fails achieve a 100% success rate over a 60-year retirement.</small>

In other words, if your portfolio can support the 3% rule, you can safely use the Dynamic SWR strategy knowing that you can always fall back to the 3% rule *as long as* you don't make overly optimistic assumptions about the future growth of your portfolio.

This demonstrates that the Dynamic SWR strategy, given reasonable parameters of market growth, demonstrates the remarkable ability to adapt to market conditions and provide reasonable assurances with regard to sequence of returns risk.

### Conclusion

If history is any indication, there will be booms and busts, and the only thing that's certain is uncertainty. My only hope is that [dynamic withdrawals](/investment-withdrawal-calculator) will help you roll with the punches.

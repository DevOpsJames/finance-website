---
layout: post
title: Five reasons why 5% is the ideal down payment on a home
excerpt: It's all about opportunity cost.
metadescription: The ideal down payment is the lowest one you can get away with, because there's lots of other things you can do with your money.
heroimage: /img/money-for-house-cropped-vertically.jpg
ogimage: https://www.nesteggly.com/img/money-for-house.jpg
ogtitle: Five reasons why 5% is the ideal down payment on a home
---

Conventional wisdom says you should save up a 20% down payment for a home. We're here to explain how that's a bunch of nonsense. Here's five reasons it's in fact better to only save up a 5% down payment:

#### Reason #1: Home price appreciation

The effective cost of owning in a home is inversely proportional to how long you own it. This is due to the relationship between transaction costs, the availability of low-interest mortgages and the steady rise in the price of real estate.

Coming up with a 20% down payment can easily take years -- years that could be spent on the good side of home price appreciation. This factor alone outweighs the perceived harm of paying PMI. We can put this theory to the test using [Nesteggly's Mortgage Calculator](/mortgage-calculator). Let's say you plan to spend 5 years in a city before changing jobs and potentially move to a new city:

**Scenario 1: [5% down payment and 5 years lived in the home](/mortgage-calculator?purchasePrice=200%2C000&downPaymentPercent=5&downPayment=10%2C000&loanAmount=190%2C000&term=30&interestRate=3.75&mortgagePayment=999&pmi=118.75&insurance=400&propertyTaxPercent=1.3&tax=2%2C600&hoa=0&totalMortgagePayment=1%2C249&standardDeduction=12%2C000&marginalTaxRate=24&interestAndPmi=713&principalPayment=286&nonPrincipalPayment=956&stateTax=5%2C000&otherDeductions=350&maintenance=1%2C000&afterTaxPayment=1%2C242&transactionCosts=6&homePriceGrowth=4&yearsOfOwnership=5&investmentOpportunityCost=7)**

Effective payment: $471

**Scenario 2: [20% down payment and 3 years lived in the home](/mortgage-calculator?purchasePrice=200%2C000&downPaymentPercent=20&downPayment=40%2C000&loanAmount=160%2C000&term=30&interestRate=3.75&mortgagePayment=741&pmi=0&insurance=400&propertyTaxPercent=1.3&tax=2%2C600&hoa=0&totalMortgagePayment=991&standardDeduction=12%2C000&marginalTaxRate=24&interestAndPmi=500&principalPayment=241&nonPrincipalPayment=794&stateTax=5%2C000&otherDeductions=350&maintenance=1%2C000&afterTaxPayment=1%2C035&transactionCosts=6&homePriceGrowth=4&yearsOfOwnership=3&investmentOpportunityCost=7)**

Effective payment: $574.

Yes, your effective payment is in fact *higher* when you put 20% down simply due to the fact you'll be spending fewer years in the home and therefore have less time to benefit from home price appreciation.

{% include adsense-in-article.html %}

#### Reason #2: You'll be able to fully fund your retirement

One problem with coming up with a massive pile of cash quickly is that you'll probably need to cannibalize other financial goals. For many, this means not fully funding retirement accounts. Due to the nature of annual limits on retirement contributions, these lost years can't ever be made up.

This problem is further exacerbated by the fact that cannibalizing retirement contributions is an inefficient way to boost your paycheck. Because 401k contributions aren't taxed, boosting your paycheck by $500/month requires you to forego $750/month in 401k contributions.

Say a 26-year-old "saves up" $12,000 in cash by postponing contributing to their 401k by 2 years. We can observe the effects of this decision using [Nesteggly's Retirement Calculator](/retirement-calculator):

**Scenario 1: [28 years old with $0 in their retirement account](/retirement-calculator?desiredLifestyle=50%2C000&currentAge=28&retirementAge=60&pensionAge=67&deathAge=95&currentSavings=0&roi=7&inflation=2&retirementIncomeTaxRate=15&socialSecurityIncome=18%2C000&fixedIncome=0)**

**Scenario 2: [28 years old with $18,000 in their retirement account](/retirement-calculator?desiredLifestyle=50%2C000&currentAge=28&retirementAge=60&pensionAge=67&deathAge=95&currentSavings=18%2C000&roi=7&inflation=2&retirementIncomeTaxRate=15&socialSecurityIncome=18%2C000&fixedIncome=0)**

In order to make up for this missed opportunity, they'll need to *permamently* increase their retirement contributions by nearly $100/month throughout their entire career. Indeed, coming up with a $12,000 pile of cash will cost nearly $40,000 over the long run.

{% include adsense-in-article.html %}

#### Reason #3: You'll feel less pressure to buy

After having spent years on the sidelines cobbling together a massive 20% down payment, you'll begin to feel a number of pressures:

* You'll be anxious to finally move out of your cramped apartment.
* You'll have passed up several opportunities to buy a suitable home at a reasonable price.
* You'll have a significant portion of your net worth earning very little interest (as of the time of writing, money market accounts accrue about 1% interest).

The FOMO is real. And a 5% down payment proportionally relieves these pressures by virtue of the fact that it's a fraction the size of a 20% down payment.

#### Reason #4: You're probably paying more to rent

Much more. If you know you're going to live in a given city for at least 5 years, the answer is a no-brainer. Owning a home is almost always cheaper than renting an equivalent one, [even assuming a modest rate of appreciation of 3%](/mortgage-calculator?purchasePrice=200%2C000&downPaymentPercent=5&downPayment=10%2C000&loanAmount=190%2C000&term=30&interestRate=3.75&mortgagePayment=999&pmi=118.75&insurance=400&propertyTaxPercent=1.3&tax=2%2C600&hoa=0&totalMortgagePayment=1%2C249&standardDeduction=12%2C000&marginalTaxRate=24&interestAndPmi=713&principalPayment=286&nonPrincipalPayment=956&stateTax=5%2C000&otherDeductions=350&maintenance=1%2C000&afterTaxPayment=1%2C242&transactionCosts=6&homePriceGrowth=3&yearsOfOwnership=5&investmentOpportunityCost=7).

#### Reason #5: You can always pay off PMI

If the thought of paying PMI keeps you up at night, you'll be delighted to know that you can remove it once you pay down your loan enough to have 20% equity. For borrowers in the early stages of their career this is particularly achievable since their income is rising quickly.

### It's all about opportunity cost

So what are you waiting for? [Fund your retirement](/retirement-calculator.html), [figure out what you can afford with a 5% down payment](/mortgage-calculator.html) and start searching for the home of your dreams.
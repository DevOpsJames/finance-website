---
layout: post
title: How to tell if mortgage rates are heading higher or lower
excerpt: In this post we reveal the secrets of the mortgage industry.
metadescription: In this post we reveal industry secrets on how to get the lowest mortgage interest rate by anticipating whether rates are rising or falling.
heroimage: /img/bank-in-hand-cropped-vertically.jpg
ogimage: https://www.nesteggly.com/img/bank-in-hand.jpg
ogtitle: How to tell if mortgage rates are heading higher or lower
published: false
---

*Updated as of March 8th, 2020*

There are essentially two rules to follow when anticipating the rise and fall of mortgage interest rates:

### Rule #1: Follow the 10-year

Mortgage rates closely track the rate of the 10-year treasury note. This is because they compete for the same sort of investors and most people don't typically hold a mortgage longer than 10 years. 

### Rule #2: Rates take the stairs down and the elevator up

When the 10-year treasury rate falls, especially to historic lows, the 30-year mortgage rate can take weeks, months, or even years to follow. The reason for this is something known as 'convexity'. Convexity describes the asymmetry of consumer behavior when mortgage rates rise and fall. 

When mortgage rates fall, demand for new mortgages skyrockets as borrowers rush to refinance into better rates. When interest rates rise, borrowers and banks typically sit tight in a sort of stalemate. This results in a sort of "lagging" effect in which mortgage rates chase the 10-year rate, especially when rates are falling to record lows.

{% include adsense-in-article.html %}

### Data dive

We can observe this phenomenon using the economic data and visualization tools provided by the Federal Reserve Bank of St. Louis (also known as the FRED). The FRED is a treasure trove of information which we'll use to look back at the summer of 2016 when interest rates fell to historic lows.

<div class="embed-container"><iframe src="https://fred.stlouisfed.org/graph/graph-landing.php?g=qiQg&width=670&height=475" scrolling="no" frameborder="0" style="overflow:hidden;" allowTransparency="true" loading="lazy"></iframe></div><script src="https://fred.stlouisfed.org/graph/js/embed.js" type="text/javascript"></script>
 
In this chart we have three data points:

* The blue line is the 30-year mortgage rate.
* The red line is the 10-year treasury rate.
* The green line measures the gap between the two.

In normal times, the gap between the two rates rarely exceeds 1.7%. When the 10-year treasury hit a historic low of 1.5% in July 2016, the gap reached a high of 1.93% and spent several months in this territory as [banks were already busy refinancing mortgages](https://www.cnbc.com/2019/04/03/mortgage-refinances-spike-39-percent-after-huge-rate-drop.html) and were reluctant to lower their rates among the flurry of refinancing.

The 10-year rebounded after Donald Trump won the 2016 presidential election as investors anticipated increased government spending and economic activity. Had the 10-year continued to remain low, it's conceivable that the 30-year mortgage rate could have gradually fallen another 25 basis points. Instead, the 30-year mortgage took the elevator up 73 basis points.

{% include adsense-in-article.html %}

### Guidance for 2020

As of March 8th the gap between the 10-year treasury and the 30-year mortgage rate has reached a 11-year high of 2.25% as fears of a global pandemic begin to take hold and send the 10-year treasury rate to [levels not seen in over 100 years](https://www.cnbc.com/2020/02/28/plunging-rates-in-the-us-are-rewriting-the-history-books.html). Mortgage rates [have already tumbled to an 8-year low](https://www.cnbc.com/2020/02/28/mortgage-rates-just-hit-8-year-low.html).

<div class="embed-container"><iframe src="https://fred.stlouisfed.org/graph/graph-landing.php?g=qiQd&width=670&height=475" scrolling="no" frameborder="0" style="overflow:hidden;" allowTransparency="true" loading="lazy"></iframe></div><script src="https://fred.stlouisfed.org/graph/js/embed.js" type="text/javascript"></script>
 
Will pandemic fears subside and cause the 10-year rise back to normal levels? Or will the 10-year remain lower long enough for the 30-year mortgage rate to fall even further? Stay tuned.
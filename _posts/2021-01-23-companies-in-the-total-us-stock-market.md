---
layout: post
title: Companies in the S&P Total U.S. Stock Market
excerpt: A comprehensive list.
metadescription: The complete list of every single company in the S&P 500 index.
heroimage: /img/landscape-egg-2-cropped-vertically.jpg
ogimage: https://www.nesteggly.com/img/landscape-egg-2-cropped-vertically.jpg
ogtitle: The complete list of companies in the S&P 500
dontshowonfrontpage: true
published: false
---

{% assign i = 0 %}
{% for thing in site.data.total-us-data %}
  {% assign i = i | plus:1 %}
{% endfor %}

The complete list of stocks in the S&P Total U.S. Stock Market index, based on the iShares Core S&P Total U.S. Stock Market ETF (ticker: ITOT). Here are the largest {{ i }} companies in the United States:

<table class="table table-striped table-responsive">
  {% for row in site.data.total-us-data %}
    {% if forloop.first %}
      <thead>
        <tr>
          {% for pair in row %}
            <th scope="col">{{ pair[0] }}</th>
          {% endfor %}
        </tr>
      </thead>
    {% endif %}
  {% endfor %}

  {% assign i = 0 %}

  {% if forloop.first == false %}
  {% assign i = i | plus:1 %}
  <tbody>
  {% endif %}
  {% for row in site.data.s-p-500-data %}
    {% tablerow pair in row %}
      {{ pair[1] }}
    {% endtablerow %}
  {% endfor %}
  {% if forloop.first == false %}
  </tbody>
  {% endif %}
</table>

---
layout: post
title: Robo-advice and the cost of convenience
excerpt: Thinking about using a robo-advisor? It could cost you.
metadescription: Robo-advisors have surged in popularity in recent years. In this post we break down why self-directed investing is still worth it.
heroimage: /img/money-computer-cropped-vertically.jpg
ogimage: https://www.nesteggly.com/img/money-computer-cropped-vertically.jpg
ogtitle: Robo-advisors and the cost of convenience
published: true
datepublished: Wednesday, December 1st, 2021
---

<div class="card bg-light mb-3">
  <div class="card-body">
    <p><b>Key points:</b></p>
    <ul>
        <li>Self-directed management remains a competitive alternative to robo-advice and traditional active management.</li>
        <li>Using a robo-advisor can delay retirement by 2-5 years compared to self-directed management.</li>
        <li>Robo-advisors will likely become more competitive in the future.</li>
    </ul>
  </div>
</div>

In 2021, [3.5 million](https://www.emarketer.com/content/young-investors-drove-robo-advisor-use) adult investors will trust a robo-advisor to manage their portfolio. When it comes to investing outside of a 401(k), the convenience offered by automated investing is undeniable. Combined with the fact that [92% of human managers underperform the S&P 500](https://www.cnbc.com/2019/03/15/active-fund-managers-trail-the-sp-500-for-the-ninth-year-in-a-row-in-triumph-for-indexing.html), it's no wonder why folks are giving the machines a chance.

You have to give the robo-advisors credit for making the process of investing so simple -- simply set up an account, configure your risk tolerance and away you go. Whatever funds are transferred in get automatically invested. Some robo-advisors even offer automated tax-loss harvesting to further juice after-tax returns.

Self-directed investing, on the other hand, is way more hands-on. You're confronted by intimidating terminology like 'bid/ask price', 'market/limit order' and so on. Being routinely exposed to your portfolio balance in this manner can invite the temptation to day trade, fat-finger a trade, or be fallen to a temporary lapse in judgement.

And even if you have nerves of steel, you have to wonder what your time is worth. As a general rule, if a machine can do it, it's probably not worth your time. Right?

In this post we put a price on that convienience and quantify what automation actually costs in 2021.

## The cost? Your life.

Using [Nesteggly's Ultimate Retirement Calculator](/retirement-calculator), we compared how many extra years you'd have to work in order to cover the cost of the most popular robo-advisors versus self-directed passive index investing. The result was staggering - robo-advisors delayed retirement anywhere from **two to five years**.

<img src="/img/robo-advisors-and-the-cost-of-convenience/extra-years-of-work.jpg" class="img-fluid" alt="Extra years of work relative to DIY management.">

To look at it another way, we calculated how much extra saving you'd have to do per year in order to overcome the effects of robo-advisor fees and stay on track to retire at the same age you would have retired with the self-directed method. We estimate you'd need sock away **an extra 11% to 37% per year**.

<img src="/img/robo-advisors-and-the-cost-of-convenience/extra-saving-to-retire-at-55.jpg" class="img-fluid" alt="Extra saving per year to retire at 55.">

We assumed the robo-advisors performed roughly the same, and that their performance mimicks that of the S&P 500 index. We also assumed robo-advisors don't ever change their current fee structure (more on that later).

## Breaking down the costs

Robo-advisors typically collect fees two ways:

1. Management fees.
2. Hidden fees. Typically, these are fees built into the funds that the robo-advisor is purchasing on your behalf. In the case of Schwab's robo-advisor, this cost manifests as a 6% cash allocation, which one of the main ways they make money (and also why they're [presently being sued](https://www.yahoo.com/now/schwab-schw-robo-advisor-business-122312204.html)).

Here's a breakdown of the fees for each robo-advisor:

<table class="table">
  <thead>
    <tr>
      <th scope="col">Robo-advisor</th>
      <th scope="col">Management fee</th>
      <th scope="col">Hidden fees</th>
      <th scope="col">Total fees</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Vanguard</th>
      <td>0.30%</td>
      <td>0.06%</td>
      <td>0.36%</td>
    </tr>
    <tr>
      <th scope="row">Wealthfront</th>
      <td>0.25%</td>
      <td>0.11%</td>
      <td>0.36%</td>
    </tr>
    <tr>
      <th scope="row">Betterment</th>
      <td>0.25%</td>
      <td>0.11%</td>
      <td>0.36%</td>
    </tr>
    <tr>
      <th scope="row">Schwab</th>
      <td>0%</td>
      <td>0.47%</td>
      <td>0.47%</td>
    </tr>
    <tr>
      <th scope="row">Future Advisor</th>
      <td>0.5%</td>
      <td>0.18%</td>
      <td>0.68%</td>
    </tr>
    <tr>
      <th scope="row">Personal Capital</th>
      <td>0.89%</td>
      <td>0.08%</td>
      <td>0.97%</td>
    </tr>
    <tr>
      <th scope="row">Traditional advisor</th>
      <td>1%</td>
      <td>0%</td>
      <td>1%</td>
    </tr>
  </tbody>
</table>

These management fees act as a 'tax' on the performance of your portfolio. So if the performance of your assets was 6% per year, a 0.5% management fee would reduce that return to 5.5%.

As a self-directed investor, you can passively invest in the S&P 500 index for as low as a 0.03% fund fee via Vanguard. Fidelity offers index funds with a 0% fee, but watch out for taxable events generated by those funds if you're holding them in a taxable account.

## Takeaways

For small sums of money, robo-advisors can still make a lot of sense. And I'd wager a lot of people fall into this category -- that awkward situation after you max your retirement accounts and have some loose change left over. In this case, using a robo is probably way more favorable than just letting it sit around in a checking account.

But when it comes to your life savings, it doesn't how much you make at your day job -- the prospect of working another 2-5 years in exchange for the convienience of a robo-advisor should make you wonder if it'd be worth going it alone. Given all the competition in this space, I anticipate it's only a matter of time before robo-advice gets cheap enough to make it a no-brainer. But in 2021, it's clear that we're simply not there yet.
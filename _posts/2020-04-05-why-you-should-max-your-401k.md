---
layout: post
title: Why you should (still) max your traditional 401k if you can
excerpt: The invention of the Roth 401k raises an age old question.
metadescription: In this post we explore why maxing your 401k always makes sense, and why it should be maxed before contributing to a Roth IRA.
heroimage: /img/landscape-egg-2-cropped-vertically.jpg
ogimage: https://www.nesteggly.com/img/landscape-egg-2.jpg
ogtitle: Why you should (still) max your traditional 401k if you can
---

<div class="card bg-light mb-3">
  <div class="card-body">
    <p>Key points:</p>
    <ul>
        <li>One aspect of retirement planning is minimizing taxes owed.</li>
        <li>The US tax system incentivizes spreading out income throughout life.</li>
        <li>You can control when you're taxed on income.</li>
    </ul>
  </div>
</div>

To tax now or tax later? That is the question.

Given the recent invention of the Roth 401k, there's been a lot of debate about whether it still makes sense to contribute to the traditional 401k. Let's dive into some of the reasons why the classic 401k option is still the reigning champ and why you should use it to the greatest extend possible.

### Reason #1: Contributing to a traditional 401k is easier on your paycheck

Since traditional 401k contributions are tax-deductible, putting $100 into your 401k only reduces your paycheck by roughly $73 (assuming your a median wage earner in the US in a state with income tax).

Roth contributions don't have this luxury. Roth 401k contributions are taxed immediately, so putting $100 into your Roth 401k will similarly reduce your paycheck by $100. It might also put you in a higher tax bracket than you otherwise would be if you made a traditional 401k contribution.

If a bigger paycheck is what you're after (or simply want to contribute more to your 401k) consider adjusting your income tax withholding by resubmitting the IRS Form W-4 you have on file with your employer. When doing this keep in mind that whatever amount saved here will eventually be owed at tax time.

{% include adsense-in-article.html %}

### Reason #2: Your tax bracket is probaby higher now

There are a few reasons for this:

* Expenses are typically lower when you're retired relative to when you're in your working years. Hence, income is also lower.
* You may end up living longer than you anticipated, so your savings might be stretched further than you expect.
* You may retire earlier than you expect.
* You might have under-saved for retirement.

While some of these factors can be mitigated with proper planning (have you tried our [ultimate retirement calculator](/retirement-calculator)?), none can be mitigated entirely. This is why it probably makes sense to defer your income taxes until you're no longer working.

To reify this concept, let's dive into an example. Take 22-year-old Johnny who's just starting to save for retirement. He landed a job out of school making $75,000, which puts him squarely in the 22% tax bracket (as of 2020 the cutoff for the 12% bracket is $40,126).

We'll compare two strategies assuming a 4% real rate of return over 30 years:

1. Maxing a Roth 401k at $19,500 per year.
2. Maxing a Traditional 401k at $19,500 per year and using the resulting $4,290 annual tax break to fund his Roth IRA.

For simplicity's sake, we'll assume the dollar amounts are in real terms.

<table class="table">
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col">Ending balance</th>
      <th scope="col">Ending balance after 15% effective tax rate</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Strategy #1</th>
      <td>Roth 401k</td>
      <td>$1,137,403</td>
      <td class="table-success">$1,137,403</td>
    </tr>
    <tr>
      <th scope="row">Strategy #2</th>
      <td>Traditional 401k</td>
      <td>$1,137,403</td>
      <td>$966,793</td>
    </tr>
    <tr>
      <th scope="row"></th>
      <td>Roth IRA</td>
      <td>$250,229</td>
      <td>$250,229</td>
    </tr>
    <tr>
      <th scope="row"></th>
      <td>Traditional 401k + Roth IRA</td>
      <td>$1,387,632</td>
      <td class="table-success">$1,217,022 <br>(7% higher)</td>
    </tr>
  </tbody>
</table>

By going the Traditional 401k + Roth IRA route, Johnny managed to boost his after-tax wealth by 7%. Good job, Johnny. 

{% include adsense-in-article.html %}

### Reason #3: You can retire early on your 401k

Ironically, one of the biggest misconceptions about 401k's happens to be one of its most compelling strengths. Yes, your 401k can in fact be tapped long before the age of 59 ½ without penalty. There are two primary ways of doing this:

#### Taking Substantially Equal Periodic Payments (SEPP)

The smart folks at the IRS have devised a very clever system for responsibly withdrawing from your 401k. They provide 3 methods of calculating a yearly payment, which you may choose according to your needs and appetite for risk:

* **Required Minimum Distribution (RMD) method:** Every year, take a withdrawal proportional to the number of remaining years you're expecting to live. This payment fluctuates from year-to-year, but the good news is you won't ever have to worry about your 401k running out.
* **Fixed amortization method:** Calculate a fixed payment based on your life expectancy and market interest rates. This payment will remain the same from year-to-year. If your portfolio experiences a downturn or you live longer than expected, it's possible that you deplete your 401k at some point while you're alive. For this reason, the IRS allows a one-time conversion to the RMD method.
* **Fixed annuitization method:** This method is so similar to the fixed amortization method, to be honest, it's not even worth getting into. All you need to know is this method may allow you to take a slightly higher payment than the amortization method.

The catch with all of these is that, once started, you can't directly control what the payment is. Additionally, you're committed to taking withdrawals for at least five years or until you're age 59 ½.

If you're interested in learning more about the SEPP methods, [the IRS has a great explainer on their website](https://www.irs.gov/retirement-plans/retirement-plans-faqs-regarding-substantially-equal-periodic-payments).

#### Doing a Roth conversion ladder

Traditional 401k's and IRA's can be converted to their Roth equivalent whenever. You pay income taxes at the time of conversion, so the trick is to time these conversions when your taxable income is lower (for example, in retirement). When you do a Roth conversion, the converted amount is available for tax-free withdrawal 5 years later.

This approach requires more record-keeping but it allows you to be more 'surgical' compared to the SEPP method. For example, in 2020 the 22% tax bracket for single filers starts at $39,476. You can convert exactly up to this amount and stay in the 12% tax bracket.

{% include adsense-in-article.html %}

### It all comes down to taxes

There you have it. Outside of very special circumstances, you can't go wrong contributing as much as you can to a traditional 401k. However, the bigger question is whether you're putting enough away overall and have a reasonable expectation what your life will be like when you retire.

Don't get me wrong. Mitigating the impact of income taxes is important. But you might not be seeing the forest for the trees unless you do a broader examination of your retirement plans. Fortunately, we have a [retirement planning calculator](/retirement-calculator) for that.
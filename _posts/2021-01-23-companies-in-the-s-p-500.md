---
layout: post
title: Companies in the S&P 500
excerpt: A comprehensive list.
metadescription: The complete list of every single company in the S&P 500 index.
heroimage: /img/landscape-egg-2-cropped-vertically.jpg
ogimage: https://www.nesteggly.com/img/landscape-egg-2-cropped-vertically.jpg
ogtitle: The complete list of companies in the S&P 500
dontshowonfrontpage: true
published: false
---

Based on the iShares Core S&P 500 ETF (ticker: IVV), here are the components of the S&P 500:

<table class="table table-striped table-responsive">
  {% for row in site.data.s-p-500-data %}
    {% if forloop.first %}
      <thead>
        <tr>
          {% for pair in row %}
            <th scope="col">{{ pair[0] }}</th>
          {% endfor %}
        </tr>
      </thead>
    {% endif %}
  {% endfor %}

  {% if forloop.first == false %}
  <tbody>
  {% endif %}
  {% for row in site.data.s-p-500-data %}
    {% tablerow pair in row %}
      {{ pair[1] }}
    {% endtablerow %}
  {% endfor %}
  {% if forloop.first == false %}
  </tbody>
  {% endif %}
</table>

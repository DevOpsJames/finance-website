---
layout: post
title: Why I Bonds are the best place for your emergency fund
excerpt: Because stuffing things under your mattress can't be great for your back.
metadescription: In this post we explore the best place for your emergency savings, and why we believe Series I Savings Bonds are better than savings accounts.
heroimage: /img/money-bags-cropped-vertically.jpg
ogimage: https://www.nesteggly.com/img/money-bags-cropped-vertically.jpg
ogtitle: Why I Bonds are the best place for your emergency fund
---

<div class="card bg-light mb-3">
  <div class="card-body">
    <p>The ideal place for an emergency fund meets the following criteria:</p>
    <ul>
        <li>Available on demand.</li>
        <li>Reasonable rate of return.</li>
        <li>No risk of loss.</li>
    </ul>
  </div>
</div>

The available options for emergency savings vehicles vary mostly on convenience and rate of return. We evaluate the trade-offs of each savings vehicle and order them from least- to most-savvy. Our least-savvy pick is:

### Cold Hard Cash

* **Available on demand?** Sure 
* **Rate of return?** Zero
* **Risk of loss?** High

Overall rating: Banana stand

<img src="/img/banana-stand.gif" class="img-fluid" alt="The cathartic burning of the banana stand.">

Recent [data](https://www.businesswire.com/news/home/20150202005113/en/Millennials-2015-Year-Milestone) suggests 43% of Americans keep their savings in cash. More than half of whom plan to store their cash in a secret location in their home. 

Despite its popularity, the cathartic burning of the banana stand in Arrested Development demonstrates a key problem with holding significant savings in cash -- it can be lit on fire. It can also be lost to theft, flood, and a myriad of other acts of god. If you're lucky and nature doesn't get to your money, inflation eventually will.

Why would you do this?

{% include adsense-in-article.html %}

### Checking Account

* **Available on demand?** Yes 
* **Rate of return?** Low
* **Risk of loss?** No

Overall rating: Decent

One of the best things about keeping your emergency fund in a checking account is that it's available at a moment's notice. It also happens to be one of the worst things since it's easier accidentally spend on non-emergency matters.

Additionally, banks put a premium on having immediate access to your money and thus provide a lower interest rate for that convenience. Since an emergency fund will (hopefully) sit unused for a long time, interest rate is an important consideration. Thus, there are better places for your emergency fund.

### Savings/Money Market Account

* **Available on demand?** Yes 
* **Rate of return?** Reasonable
* **Risk of loss?** No

Overall rating: Great

Savings and money market accounts are like checking accounts except with limits on the number of transfers you can do per month. This limitation usually comes with a key benefit: a higher interest rate.

Since you shouldn't need to frequently transfer money to and from your emergency fund, trading this feature for a higher interest rate is a no-brainer.

{% include adsense-in-article.html %}

### Our Top Pick: Series I Savings Bonds

* **Available on demand?** Yes 
* **Rate of return?** Great
* **Risk of loss?** No

Otherwise known simply as "I Bonds," this old school savings vehicle is our top pick for a bunch of reasons.

**The interest rate is great**

As of the time of writing (April 12th, 2020), [the current interest rate on Series I savings bonds](https://www.treasurydirect.gov/news/pressroom/currentibondratespr.htm) is 2.22%. 

The [average rate on a money market account](https://fred.stlouisfed.org/series/MMNRNJ) on the other hand is presently a measely 0.11%. And [savings accounts](https://fred.stlouisfed.org/series/SAVNRNJ) are even worse at a paltry 0.07%.

**Tax-deferred interest**

Pretty much every interest-bearing account requires you to pay tax each year as interest accrues. With I Bonds, tax isn't due until the bond is cashed. This allows your principal to compound tax-deferred.

And even better, it's possible to get the tax forgiven completely if the proceeds are used to fund higher education costs.

**The interest is exempt from state and local taxes**

Not much else to say here. Lower taxes are good.

**The interest rate tracks the consumer price index**

The actual mechanics of how it works are tricky, but roughly speaking the interest rate received on I Bonds tracks the consumer price index. So as inflation rises, so too will the value of the savings bond. And as an added bonus, during periods of deflation you won't lose any principal.

The formula-driven nature of I Bonds is preferable compared to the profit-driven approach used by banks to calculate the interest rate on your savings account. You may find a bank with a competitive interest rate, only to notice that the rate is considerably lower once they're no longer promoting that particular product.

**You can get I Bonds in paper form**

If you're into collectibles, paper I Bonds are a great way to channel your fondness for hoarding strange things. They come in a variety of denominations and feature historically significant figures such as Albert Einstein and Dr. Martin Luther King Jr.

<img src="/img/paper-i-bond.gif" class="img-fluid" alt="A paper I Bond.">

Paper I Bonds, unlike cash, are only redeemable by the holder and can be replaced if stolen or destroyed. Pretty cool stuff.

**The catch? They're not redeemable within the first year**

This is a somewhat significant downside and something you should certainly plan for, but this doesn't dissuade us from selecting I Bonds as our top pick. After the first year, they're redeemable in as little as a few days in my experience.

If you're similarly not dissuaded, you can head on over to [treasurydirect.gov](https://www.treasurydirect.gov/indiv/research/indepth/ibonds/res_ibonds.htm) and get yourself some I Bonds.
![Purge CloudFlare cache](https://github.com/jamejone/finance-website/workflows/Purge%20CloudFlare%20cache/badge.svg)

## Setup

* Clone the Repo
* `npm install`
* `bundle install`
* To build for dev: `npm run dev`
* To build for production: `npm run prod`

Building for dev provides auto-browser refresh for JavaScript and Jekyll content and additionally provides debugging capabilities in Chrome dev tools.
